import React, { useState } from "react";
import Header from "./containers/Header";
import { motion } from "framer-motion";
import SideContainer from "./containers/SideContainer";
import { MainStyle } from "./styles/MainStyle";
import WeeksContainer from "./containers/WeeksContainer";
import TodayContainer from "./containers/TodayContainer";
import { MainContainerStyle } from "./styles/MainContainerStyle";
import { BottomContainerStyle } from "./styles/BottomContainerStyle";
import { WeeksHeaderStyle } from "./styles/WeeksHeaderStyle";

function App() {
  const [selectedWeekCardIndex, setSelectedWeekCardIndex] = useState(0);
  const weekCardClickHandler = (index: number): void => {
    setSelectedWeekCardIndex(index);
  };

  return (
    <MainStyle>
      <Header />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        className="sideContainer"
      >
        <SideContainer />
      </motion.div>
      <MainContainerStyle>
        <TodayContainer
          hideHeader={false}
          selectedWeekCardIndex={selectedWeekCardIndex}
        />
      </MainContainerStyle>

      <BottomContainerStyle>
        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
          <WeeksHeaderStyle>
            <h1>Forecast</h1>
          </WeeksHeaderStyle>
          <WeeksContainer
            clickHandler={weekCardClickHandler}
            selectedCardIndex={selectedWeekCardIndex}
          />
        </motion.div>
      </BottomContainerStyle>
    </MainStyle>
  );
}

export default App;

import { takeLatest, call, put, fork } from "redux-saga/effects";
import { fetchWeatherApi } from "./weather/api";
import {
  FETCH_WEATHER_DATA,
  WEATHER_DATA_RECEIVED,
  WEATHER_FETCH_FAILED,
} from "./constants/actions";
import { weatherActionType } from "./constants/types";

function* getWeather(action: weatherActionType) {
  const { city } = action.payload;
  const { response, error } = yield call(fetchWeatherApi, city);

  if (response) yield put({ type: WEATHER_DATA_RECEIVED, payload: response });
  else yield put({ type: WEATHER_FETCH_FAILED, error });
}

function* weatherSagaWatcher() {
  //reacts to the latest action FETCH_WEATHER_DATA
  yield takeLatest(FETCH_WEATHER_DATA, getWeather);
}

export default function* rootSaga() {
  yield fork(weatherSagaWatcher);
}

type weatherActionPayloadType = {
  city: string;
};

export type weatherActionType = {
  type: string;
  payload: weatherActionPayloadType;
};

export type weatherStateType = {
  weather?: object;
  isLoading: boolean;
  error?: Error;
};

export type fetchResponseActionType = {
  type: string;
  payload?: object;
  error?: Error;
};

import { combineReducers } from "redux";
import { createSelector } from "reselect";
import { reduceWeather } from "./weather/reducer";

const reducers = combineReducers({
  weatherData: reduceWeather,
});

const selectIsWeatherLoading = (state: any) => {
  return state.isLoading;
};

const selectNthWeatherFromToday = (n: number, state: any) => {
  return { ...state?.weather?.data?.list[n] };
};

const selectCityDetails = (state: any) => {
  return { ...state?.weather?.data?.city };
};

export const selectors = {
  isWeatherLoading: createSelector(
    (state: any) => selectIsWeatherLoading(state.weatherData),
    (isLoading) => isLoading
  ),
  weatherToday: createSelector(
    (state: any) => selectNthWeatherFromToday(0, state.weatherData),
    (weather) => weather
  ),
  futureWeather: (n: number, state: any) => {
    return createSelector(
      (state: any) => selectNthWeatherFromToday(n, state.weatherData),
      (weather) => weather
    )(state);
  },
  cityDetails: createSelector(
    (state: any) => selectCityDetails(state.weatherData),
    (details) => details
  ),
};

export default reducers;

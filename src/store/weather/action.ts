import { FETCH_WEATHER_DATA } from "../constants/actions";
import { weatherActionType } from "../constants/types";

const WeatherActions = {
  fetchWeather: (city: string): weatherActionType => {
    return {
      type: FETCH_WEATHER_DATA,
      payload: {
        city: city,
      },
    };
  },
};

export default WeatherActions;

import axios from "axios";

async function fetchWeather(city: string) {
  try {
    const response = await axios.get(process.env.REACT_APP_API_URL as string, {
      params: {
        q: city,
        appid: process.env.REACT_APP_API_KEY,
        units: "metric",
        mode: "json",
      },
    });

    return response;
  } catch (error) {
    const errorMessage = {
      error: "City doesn't exists",
    };
    return errorMessage;
  }
}

export function fetchWeatherApi(city: string) {
  return fetchWeather(city)
    .then((response) => ({ response }))
    .catch((error) => ({ error }));
}

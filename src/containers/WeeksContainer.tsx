import React from "react";
import { connect } from "react-redux";
import { motion } from "framer-motion";
import WeekCard from "../components/WeekCard";
import { selectors } from "../store/reducers";
import { WeeksContainerStyle } from "../styles/WeeksContainerStyle";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import { WeekCardStyle } from "../styles/WeekCardStyle";
import PropTypes from "prop-types";

type weekInfoType = {
  weekDay?: string;
  logoTag?: string;
  temp?: number;
};

const WeeksContainer = ({
  weeks,
  clickHandler,
  selectedCardIndex,
}: {
  weeks: weekInfoType[];
  clickHandler: (index: number) => void;
  selectedCardIndex: number;
}) => {
  return (
    <WeeksContainerStyle>
      {weeks.map((week, index) => {
        return weekCardRenderer(week, index, clickHandler, selectedCardIndex);
      })}
    </WeeksContainerStyle>
  );
};

function weekCardRenderer(
  week: weekInfoType,
  index: number,
  clickHandler: (index: number) => void,
  selectedCardIndex: number
): JSX.Element {
  if (!!week.weekDay && !!week.logoTag && week.hasOwnProperty("temp")) {
    return (
      <WeekCard
        key={index}
        day={week.weekDay || ""}
        imgSrc={`http://openweathermap.org/img/wn/${week.logoTag}@2x.png`}
        temp={week.temp || 0}
        index={index}
        clickHandler={clickHandler}
        selected={index === selectedCardIndex}
      />
    );
  } else {
    return (
      <WeekCardStyle selected={false} key={index}>
        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
          <Loader type="ThreeDots" color="lightGrey" height={100} width={100} />
        </motion.div>{" "}
      </WeekCardStyle>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    weeks: ((): weekInfoType[] => {
      return [...Array(7).keys()].map((index: number) => {
        let weather = selectors.futureWeather(index, state);
        return {
          weekDay:
            weather?.dt &&
            Intl.DateTimeFormat("EN-UK", {
              weekday: "short",
            }).format(new Date(weather.dt * 1000)),
          logoTag: !!weather && !!weather.weather && weather.weather[0]?.icon,
          temp: weather?.temp?.day,
        };
      });
    })(),
  };
}

export default connect(mapStateToProps)(WeeksContainer);

WeeksContainer.propTypes = {
  weeks: PropTypes.any.isRequired,
  clickHandler: PropTypes.func.isRequired,
  selectedCardIndex: PropTypes.number.isRequired,
};

weekCardRenderer.propTypes = {
  week: PropTypes.shape({
    weekDay: PropTypes.string.isRequired,
    logoTag: PropTypes.string.isRequired,
    temp: PropTypes.number.isRequired,
  }),
  index: PropTypes.number.isRequired,
  clickHandler: PropTypes.func,
  selectedCardIndex: PropTypes.number.isRequired,
};

import React from "react";
import { connect } from "react-redux";
import HighlightCard from "../components/HighlightCard";
import { selectors } from "../store/reducers";
import { TodayContainerStyle } from "../styles/TodayContainerStyle";
import { formatTime } from "../utils/formatTime";
import { capitalize } from "../utils/capitalize";
import { motion } from "framer-motion";
import ReactPlaceholder from "react-placeholder";
import "react-placeholder/lib/reactPlaceholder.css";
import PropTypes from "prop-types";

const speed_unit = "meters/s";
const temp_unit = "C";

type propsType = {
  hideHeader: boolean;
  weatherToday?: any;
  weeks?: any[];
  selectedWeekCardIndex?: number;
};

const TodayContainer = ({
  hideHeader,
  weatherToday,
  weeks,
  selectedWeekCardIndex = 0,
}: propsType) => {
  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
      <TodayContainerStyle>
        {renderHighlightHeading(selectedWeekCardIndex, weeks)}
        {hideHeader
          ? renderHighlightCards(weatherToday)
          : renderHighlightCards(weeks && weeks[selectedWeekCardIndex])}
      </TodayContainerStyle>
    </motion.div>
  );
};

function renderHighlightHeading(
  selectedWeekCardIndex: number,
  weeks: any
): JSX.Element {
  if (selectedWeekCardIndex === 0)
    return (
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: 0.1 }}
      >
        <h2>Today's Weather</h2>
      </motion.div>
    );
  const date = new Date(weeks[selectedWeekCardIndex].dt * 1000);
  const weekDay = Intl.DateTimeFormat("EN-UK", {
    weekday: "long",
  }).format(date);
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ delay: 0.1 }}
    >
      <h2>{capitalize(weekDay)}'s Weather</h2>
    </motion.div>
  );
}

function renderHighlightCards(weather: any): JSX.Element {
  return (
    <TodayContainerStyle>
      <ReactPlaceholder
        showLoadingAnimation
        type="text"
        ready={
          weather.hasOwnProperty("temp") && weather.temp.hasOwnProperty("day")
        }
        rows={1}
        style={{ width: "250px", padding: "5px" }}
      >
        <HighlightCard
          position={1}
          title="Temperature"
          primaryInfo={`${Math.round(weather?.temp?.day)}° ${temp_unit}`}
        />
      </ReactPlaceholder>

      <ReactPlaceholder
        showLoadingAnimation
        type="text"
        ready={
          weather.hasOwnProperty("temp") && weather.temp.hasOwnProperty("morn")
        }
        rows={1}
        style={{ width: "250px", padding: "5px" }}
      >
        <HighlightCard
          position={2}
          title="Sunrise"
          primaryInfo={`${weather.sunrise && formatTime(weather?.sunrise)}`}
        />
      </ReactPlaceholder>

      <ReactPlaceholder
        showLoadingAnimation
        type="text"
        ready={
          !!weather.hasOwnProperty("temp") && weather.temp.hasOwnProperty("eve")
        }
        rows={1}
        style={{ width: "250px", padding: "5px" }}
      >
        <HighlightCard
          position={3}
          title="Sunset"
          primaryInfo={`${
            weather && weather.sunset && formatTime(weather?.sunset)
          }`}
        />
      </ReactPlaceholder>

      <ReactPlaceholder
        showLoadingAnimation
        type="text"
        ready={weather.hasOwnProperty("pressure")}
        rows={1}
        style={{ width: "250px", padding: "5px" }}
      >
        <HighlightCard
          position={4}
          title="Pressure"
          primaryInfo={`${Math.round(weather?.pressure)} hPa`}
        />
      </ReactPlaceholder>

      <ReactPlaceholder
        showLoadingAnimation
        type="text"
        ready={weather.hasOwnProperty("humidity")}
        rows={1}
        style={{ width: "250px", padding: "5px", paddingBottom: "10px" }}
      >
        <HighlightCard
          position={5}
          title="Humidity"
          primaryInfo={`${Math.round(weather?.humidity)} %`}
        />
      </ReactPlaceholder>

      <ReactPlaceholder
        showLoadingAnimation
        type="text"
        ready={weather.hasOwnProperty("speed") && weather.hasOwnProperty("deg")}
        rows={1}
        style={{ width: "250px", padding: "5px", paddingBottom: "10px" }}
      >
        <HighlightCard
          position={6}
          title="Wind Status"
          primaryInfo={`${Math.round(weather?.speed)} ${speed_unit}`}
        />
      </ReactPlaceholder>
    </TodayContainerStyle>
  );
}

function mapStateToProps(state: any) {
  return {
    weatherToday: selectors.weatherToday(state),
    weeks: (() => {
      return [...Array(7).keys()].map((index: number) =>
        selectors.futureWeather(index, state)
      );
    })(),
  };
}

export default connect(mapStateToProps)(TodayContainer);

TodayContainer.propTypes = {
  hideHeader: PropTypes.any,
  weatherToday: PropTypes.shape({
    weather: PropTypes.array,
    dt: PropTypes.number,
    temp: PropTypes.shape({
      day: PropTypes.number,
      morn: PropTypes.number,
      eve: PropTypes.number,
    }),
  }),
  weeks: PropTypes.array,
  selectedWeekCardIndex: PropTypes.number.isRequired,
};

renderHighlightHeading.propTypes = {
  selectedWeekCardIndex: PropTypes.number.isRequired,
  weeks: PropTypes.array.isRequired,
};

renderHighlightCards.propTypes = {
  weather: PropTypes.shape({
    temp: PropTypes.shape({
      day: PropTypes.string.isRequired,
      morn: PropTypes.string.isRequired,
    }),
    speed: PropTypes.string.isRequired,
    deg: PropTypes.string.isRequired,
    dt: PropTypes.number.isRequired,
    humidity: PropTypes.string.isRequired,
    pressure: PropTypes.string.isRequired,
  }),
};

import React from "react";
import { connect } from "react-redux";
import { selectors } from "../store/reducers";
import { SideContainerStyle } from "../styles/SideContainerStyle";
import { capitalize } from "../utils/capitalize";
import ReactPlaceholder from "react-placeholder";
import "react-placeholder/lib/reactPlaceholder.css";
import { motion } from "framer-motion";

const variants = {
  hidden: { opacity: 0 },
  show: { opacity: 1 },
};

interface ICityDetails {
  name: string;
}

interface IWeatherToday {
  name: string;
  dt: number;
  temp: {
    day: number;
  };
  weather: [any];
}

const SideContainer = ({
  cityDetails,
  weatherToday,
}: {
  cityDetails: ICityDetails;
  weatherToday: IWeatherToday;
}) => {
  return (
    <SideContainerStyle>
      <section className="cityPart">
        <ReactPlaceholder
          showLoadingAnimation
          type="text"
          ready={!!cityDetails && !!cityDetails.name}
          rows={1}
          style={{ width: "150px" }}
        >
          <motion.div
            initial="hidden"
            animate="show"
            variants={variants}
            transition={{ delay: 0.2 }}
          >
            <h2>{cityDetails.name}</h2>
          </motion.div>
        </ReactPlaceholder>

        <ReactPlaceholder
          showLoadingAnimation
          type="text"
          ready={!!weatherToday && !!weatherToday.dt}
          rows={1}
          style={{ width: "280px", paddingTop: "10px" }}
        >
          {!!weatherToday.dt && (
            <motion.div
              initial="hidden"
              animate="show"
              variants={variants}
              transition={{ delay: 0.4 }}
            >
              <p>
                {capitalize(
                  Intl.DateTimeFormat("en-UK", {
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                    weekday: "long",
                  }).format(new Date(weatherToday.dt * 1000))
                )}
              </p>
            </motion.div>
          )}
        </ReactPlaceholder>
      </section>
      <section className="main">
        <ReactPlaceholder
          showLoadingAnimation
          type="media"
          ready={!!weatherToday?.weather && !!weatherToday?.weather[0]?.icon}
          rows={0}
          style={{ width: "100px" }}
        >
          <motion.div
            initial="hidden"
            animate="show"
            variants={variants}
            transition={{ delay: 0.6 }}
          >
            <div>
              <img
                src={`http://openweathermap.org/img/wn/${
                  weatherToday?.weather && weatherToday?.weather[0]?.icon
                }@2x.png`}
                alt="weather icon"
                style={{ width: "150px" }}
              />
            </div>
          </motion.div>
        </ReactPlaceholder>

        <div className="metrics">
          <ReactPlaceholder
            showLoadingAnimation
            type="text"
            ready={!!weatherToday?.weather && !!weatherToday?.weather[0]?.icon}
            rows={2}
            style={{ width: "100px", paddingBottom: "15px" }}
          >
            {" "}
            <motion.div
              initial="hidden"
              animate="show"
              variants={variants}
              transition={{ delay: 0.8 }}
            >
              <div className="temp">
                {Math.round(!!weatherToday?.weather && weatherToday.temp.day)}
                &#176;C
              </div>
            </motion.div>
          </ReactPlaceholder>

          <ReactPlaceholder
            showLoadingAnimation
            type="text"
            ready={
              !!weatherToday.weather && !!weatherToday.weather[0].description
            }
            rows={1}
            style={{ width: "100px" }}
          >
            <motion.div
              initial="hidden"
              animate="show"
              variants={variants}
              transition={{ delay: 1 }}
            >
              <p className="description">
                {capitalize(
                  !!weatherToday.weather && weatherToday.weather[0].description
                )}
              </p>
            </motion.div>
          </ReactPlaceholder>
        </div>
      </section>
    </SideContainerStyle>
  );
};

function mapStateToProps(state: any) {
  return {
    cityDetails: selectors.cityDetails(state),
    weatherToday: selectors.weatherToday(state),
  };
}

export default connect(mapStateToProps)(SideContainer);

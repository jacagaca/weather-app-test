import React, { useEffect, useState } from "react";
import { useDispatch, connect, useSelector } from "react-redux";
import { selectors } from "../store/reducers";
import { HeaderStyle } from "../styles/HeaderStyle";
import { useForm } from "react-hook-form";
import WeatherActions from "../store/weather/action";
import { ErrorBox } from "../components/ErrorBox";
import { motion } from "framer-motion";
import Geocode from "react-geocode";
Geocode.setApiKey("AIzaSyA5QfRxcuQjReMTXzDdecH-soWbQKpWxO8");

const errorMessageConst = "City doesn't exists";
const requiredConst = "required";
const initialPosition = {
  lat: 0,
  long: 0,
};
const Header = () => {
  const dispatch = useDispatch();
  const [position, setPosition] = useState(initialPosition);
  const [positionChecked, setPositionChecked] = useState(false);
  const [city, setCity] = useState("Opole");
  const weatherData: any = useSelector((state: any) => state.weatherData);

  const { register, errors, handleSubmit } = useForm();

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition(function (position) {
        setPosition({
          lat: position.coords.latitude,
          long: position.coords.longitude,
        });

        setPositionChecked(true);
      });
    }
  }, []);

  useEffect(() => {
    if (position && !positionChecked) {
      const lat = position.lat.toString();
      const long = position.long.toString();

      Geocode.fromLatLng(lat, long).then(
        (response) => {
          const city = response.results[0].address_components[3].long_name;
          setCity(city);
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }, [position]);

  useEffect(() => {
    dispatch(WeatherActions.fetchWeather(city));
  }, [city]);

  interface Data {
    city: string;
  }

  const onSubmit = (data: Data) => {
    return dispatch(WeatherActions.fetchWeather(data.city));
  };

  return (
    <HeaderStyle>
      <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
        <section className="logo">
          <h1>Weather App</h1>
        </section>
        <section className="search">
          <div className="wrapper">
            <div className="container">
              <form
                role="search"
                method="get"
                className="search-form form"
                action=""
                onSubmit={handleSubmit(onSubmit)}
              >
                <label>
                  <span className="screen-reader-text">Search for...</span>

                  <input
                    className="search-field"
                    name="city"
                    type="search"
                    placeholder="Search for places ..."
                    ref={register({
                      required: true,
                      minLength: 1,
                      maxLength: 30,
                      // pattern: /^[A-Za-z]+$/i,
                    })}
                  />
                  {errors?.city && errors?.city?.type === requiredConst && (
                    <ErrorBox>Please enter city name</ErrorBox>
                  )}
                  {weatherData?.weather &&
                    weatherData?.weather?.error === errorMessageConst && (
                      <ErrorBox>Please enter correct city name</ErrorBox>
                    )}
                </label>
                <button className="search-submit" type="submit">
                  <i className="fa fa-search fa-lg"></i>
                </button>
              </form>
            </div>
          </div>
        </section>
      </motion.div>
    </HeaderStyle>
  );
};

function mapStateToProps(state: any) {
  return {
    cityDetails: selectors.cityDetails(state),
  };
}

export default connect(mapStateToProps)(Header);

import styled from "styled-components";

export const HeaderStyle = styled.header`
  grid-area: header;

  @media (max-width: 880px) {
    padding-top: 30px;
  }

  .logo {
    position: absolute;
    top: 0;
    left: 0;

    h1 {
      color: white;
      text-transform: uppercase;
      font-size: 1.5rem;
      padding: 10px;
      margin: 0;
      font-family: "Lucida Console", Courier, monospace;
    }
  }
  .wrapper {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .container {
    width: 60%;
  }

  input[type="search"] {
    background-color: white;
    vertical-align: middle;
    border-radius: 0.25rem;
    border: 1px solid ${(props) => props.theme.whiteColor};
    font-size: 1rem;
    width: 100%;
    line-height: 2;
    padding-left: 1rem;
    padding-top: 0.3rem;
    padding-bottom: 0.3rem;
    -webkit-transition: border-color 0.2s;
    -moz-transition: border-color 0.2s;
    transition: border-color 0.2s;
    border-radius: 50px;
    box-shadow: 3px 4px 12px 0px ${(props) => props.theme.grayOpacity};
  }

  .message-box {
    font-size: 0.8rem;
    width: 300px;
    border-radius: 6px;
    position: absolute;
    right: 10px;
    top: 10px;

    i {
      vertical-align: middle;
      padding: 20px;
      &.exit-button {
        float: right;
        opacity: 0.4;
        &:hover {
          opacity: 0.8;
        }
      }
    }
  }
  .message-text {
    vertical-align: middle;
  }

  .message-box-error {
    background-color: ${(props) => props.theme.lightRedColor};
    border: darken(${(props) => props.theme.lightRedColor}, 20%) 2px solid;
    color: ${(props) => props.theme.darkRedColor};
  }

  form.search-form {
    display: flex;
    justify-content: center;
  }

  label {
    flex-grow: 1;
    flex-shrink: 0;
    flex-basis: auto;
    align-self: center;
  }

  input.search-field {
    padding-left: 30px;
    flex-grow: 1;
    flex-shrink: 0;
    flex-basis: auto;
    align-self: center;
    height: auto;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }

  input.search-submit {
    height: auto;

    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    border-top-right-radius: 50px;
    border-bottom-right-radius: 50px;
    font-size: 1rem;
  }
  input:focus {
    outline: none;
  }

  .screen-reader-text {
    clip: rect(1px, 1px, 1px, 1px);
    position: absolute !important;
    height: 1px;
    width: 1px;
    overflow: hidden;
  }

  button.search-submit {
    width: 80px;
    height: 44px;
    margin-top: 5px;
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    border-top-right-radius: 50px;
    border-bottom-right-radius: 50px;
    font-weight: 600;
    font-size: 0.8rem;
    line-height: 1.15;
    letter-spacing: 0.1rem;
    text-transform: uppercase;
    background: ${(props) => props.theme.orangeColor};
    color: ${(props) => props.theme.black};
    border: 1px solid transparent;
    vertical-align: middle;
    text-shadow: none;
    -webkit-transition: all 0.2s;
    -moz-transition: all 0.2s;
    transition: all 0.2s;
  }

  button.search-submit {
    box-shadow: 1px 4px 12px 0px ${(props) => props.theme.grayOpacity};
  }

  button.search-submit:hover,
  button.search-submit:active,
  button.search-submit:focus {
    cursor: pointer;
    background: ${(props) => props.theme.darkOrangeColor};
    outline: none;
  }
`;

import styled from "styled-components";

export const TodayContainerStyle = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: auto;
  align-items: center;
  justify-content: center;
  color: white;

  @media (max-width: 450px) {
    flex-direction: column;

  h2 {
    font-size: 1.6rem;
    font-weight: 600;
    text-align: center;
  }
`;

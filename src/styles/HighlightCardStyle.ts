import styled from "styled-components";

export const HighlightCardStyle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  flex-basis: 30%;
  margin: 5px;
  font-size: 0.8rem;
  padding-top: 5px;
  padding-bottom: 10px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  .card {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  p {
    margin: 0;
    padding: 0;
  }
  h1 {
    margin: 0;
    padding: 0;
  }
`;

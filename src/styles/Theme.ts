export type themeType = {
  blackColor: string;
  blueColor: string;
  lightGrayColor: string;
  orangeColor: string;
  darkOrangeColor: string;
  whiteColor: string;
  lightRedColor: string;
  darkRedColor: string;
  grayOpacity: string;
};

export const theme: themeType = {
  blackColor: "#070707",
  blueColor: "#4050d2",
  lightGrayColor: "#f3f3f4",
  orangeColor: "#ffbf5e",
  whiteColor: "#ffffff",
  darkOrangeColor: "#ff8c00",
  lightRedColor: "#ecc8c5",
  darkRedColor: "#b83c37",
  grayOpacity: "rgba(201, 201, 201, 1)",
};

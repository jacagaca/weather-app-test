import styled from "styled-components";

export const WeeksContainerStyle = styled.section`
  width: 100%;
  display: flex;
  overflow-x: scroll;
`;

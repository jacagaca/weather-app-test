import styled from "styled-components";

export const SideContainerStyle = styled.div`
  @media (max-width: 880px) {
    padding-top: 0px;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  @media (max-width: 640px) {
    padding-top: 0px;
    margin: auto;
    display: block;
    justify-content: center;
    align-items: center;
  }
  .cityPart {
    padding-top: 5px;
    padding-left: 5px;

    h2 {
      font-size: 2rem;
      margin: 0;
      padding: 0;
    }
    p {
      font-size: 1.2rem;
      margin: 0;
      padding: 0;
    }
  }

  .main {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0.5rem;
    padding-top: 1rem;
    .img {
      width: 200px;
      height: 200px;
    }
    .metrics {
      width: 100%;
      min-height: 5rem;
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      .temp {
        margin: 0;
        padding: 0;
        font-size: 3rem;
        min-height: 5rem;
      }
      .description {
        font-size: 1.3rem;
        min-height: 5rem;
        margin: 0;
        padding: 0;
      }
    }
  }
`;

import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`


*, *:before, *:after{
  box-sizing: inherit
}

html {
  padding-top: 1rem;
  box-sizing: border-box;
  background:
        linear-gradient(
          rgba(0, 0, 0, 0.4), 
          rgba(0, 0, 0, 0.4)
        ),
        url("backgroundImage.jpg") no-repeat center center fixed;
  font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;      
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  background: rgba(black, 0.5);
}



 body {
  padding: 0;
  margin: 0;
  font-size: 1.5rem;
  line-height: 2;
}

`;

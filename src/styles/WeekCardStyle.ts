import styled from "styled-components";
interface Props {
  theme: {
    whiteColor: string;
    orangeColor: string;
    lightGreyColor: string;
  };
  selected: boolean | undefined;
}

export const WeekCardStyle = styled.div`
  .card {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
  }

  background-color: ${(props: Props) =>
    props.selected ? "rgba(255,153,51, 0.4)" : "rgba(214, 214, 214, 0.4)"};
  color: white;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 0.5rem;
  margin: 0.5rem;
  flex-basis: 13%;
  height: 150px;

  img {
    height: 50px;
    padding: 0;
    margin: 0;
  }

  p {
    padding: 0;
    margin: 0;

    font-size: 1.3rem;
    font-weight: 600;
  }

  &:hover {
    cursor: pointer;
  }
`;

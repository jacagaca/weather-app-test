import styled from "styled-components";

export const MainContainerStyle = styled.div`
  grid-area: main;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50px;
  background-color: rgba(214, 214, 214, 0.4);
`;

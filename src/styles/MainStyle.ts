import styled from "styled-components";

export const MainStyle = styled.div`
  width: 90%;
  margin: auto;
  margin-top: 50px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  grid-template-rows: 80px auto auto auto;
  grid-template-areas:
    "header header header header header"
    "side side main main main"
    "bottom bottom bottom bottom bottom"
    "bottom bottom bottom bottom bottom";
  grid-gap: 10px;

  @media (max-width: 880px) {
    grid-template-columns: 1fr;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-template-rows: 120px auto auto auto;
    grid-template-areas:
      "header header header header header"
      "side side side side side"
      "main main main main main"
      "bottom bottom bottom bottom bottom";
  }

  .sideContainer {
    grid-area: side;
    color: white;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding-top: 20px;
  }
`;

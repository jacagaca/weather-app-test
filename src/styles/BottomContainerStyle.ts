import styled from "styled-components";

export const BottomContainerStyle = styled.div`
  grid-area: bottom;
  display: flex;
  flex-direction: column;
  @media (max-width: 880px) {
    padding-bottom: 20px;
  }
`;

import styled from "styled-components";

export const WeeksHeaderStyle = styled.section`
  padding-left: 10px;

  @media (max-width: 880px) {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  h1 {
    margin: 0;
    font-size: 1.4rem;
    color: white;
  }
`;

import React from "react";
import { WeekCardStyle } from "../styles/WeekCardStyle";
import { motion } from "framer-motion";
import PropTypes from "prop-types";

type propsType = {
  day: string;
  imgSrc: string;
  temp: number;
  index: number;
  clickHandler: (index: number) => void;
  selected?: boolean;
};

const WeekCard = ({
  day,
  imgSrc,
  temp,
  index,
  clickHandler,
  selected,
}: propsType) => {
  const handleClick = () => {
    clickHandler(index);
  };

  return (
    <WeekCardStyle onClick={handleClick} selected={selected}>
      <motion.div
        className="card"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: index * 0.2 }}
      >
        <p>{day}</p>
        <img src={imgSrc} alt="icon" />
        <p>{Math.round(temp)}&#176;</p>
      </motion.div>
    </WeekCardStyle>
  );
};

export default WeekCard;

WeekCard.propTypes = {
  selected: PropTypes.bool.isRequired,
  day: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  temp: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  clickHandler: PropTypes.func.isRequired,
};

import React from "react";
import { motion } from "framer-motion";
import PropTypes from "prop-types";

interface Props {}

export const ErrorBox: React.FC<Props> = (props) => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      key="errorBox"
    >
      <div className="message-box message-box-error" role="dialog">
        <i className="fa fa-ban fa-2x"></i>
        <span className="message-text">
          <strong>Error:</strong> {props.children}
        </span>
      </div>
    </motion.div>
  );
};

ErrorBox.propTypes = {
  children: PropTypes.element,
};

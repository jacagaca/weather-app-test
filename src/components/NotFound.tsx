import React from "react";

interface Props {}
export const NotFound: React.FC<Props> = () => {
  return (
    <>
      <div>
        <h1>Page not found - 404</h1>
      </div>
    </>
  );
};

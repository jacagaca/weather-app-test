import React from "react";
import { HighlightCardStyle } from "../styles/HighlightCardStyle";
import { motion } from "framer-motion";
import PropTypes from "prop-types";

type propsType = {
  title: string;
  primaryInfo: string;
  position: number;
};

const HighlightCard = ({ title, primaryInfo, position }: propsType) => {
  return (
    <HighlightCardStyle>
      <motion.div
        className="card"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: position * 0.2 }}
      >
        <p className="title">{title}</p>
        <h1>{primaryInfo}</h1>
      </motion.div>
    </HighlightCardStyle>
  );
};

export default HighlightCard;

HighlightCard.propTypes = {
  title: PropTypes.string.isRequired,
  primaryInfo: PropTypes.string.isRequired,
  position: PropTypes.number.isRequired,
};

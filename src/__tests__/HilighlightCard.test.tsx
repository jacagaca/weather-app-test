import React from "react";
import { render, screen } from "@testing-library/react";
import HighlightCard from "../components/HighlightCard";

describe("components/HighlightCard", () => {
  test("renders properly", () => {
    const props = {
      title: "highlight card title",
      primaryInfo: "primary info",
      position: 1,
    };
    render(<HighlightCard {...props} />);
    expect(screen.getByText(props.title)).toBeInTheDocument();
    expect(screen.getByText(props.primaryInfo)).toBeInTheDocument();
  });
});

import React from "react";
import { render, screen } from "@testing-library/react";
import { NotFound } from "../components/NotFound";

describe("components/NotFound", () => {
  test("renders properly", () => {
    const props = {};
    render(<NotFound {...props} />);

    expect(screen.getByRole("heading")).toHaveTextContent(
      "Page not found - 404"
    );
  });
});

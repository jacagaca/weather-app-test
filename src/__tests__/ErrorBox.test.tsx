import React from "react";
import { render, screen } from "@testing-library/react";
import { ErrorBox } from "../components/ErrorBox";

describe("components/ErrorBox", () => {
  test("renders properly", () => {
    const props = {};
    render(<ErrorBox {...props} />);
    // screen.debug();
    expect(screen.getByRole("dialog", { hidden: true })).toHaveTextContent(
      "Error:"
    );
  });
});

export function formatTime(timestamp: number): string {
  return Intl.DateTimeFormat(undefined, {
    hour: "numeric",
    minute: "numeric",
  }).format(new Date(timestamp * 1000));
}
